;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; Change title screen to options
; ---------------------------------------------------------------------------

    org $4EC3E
    dc.l PermanentMenu

    ;org $2568
    ;jmp PermanentMenu

    ; change title music to provided
    org $2AFA
    move.w #$1A,d7 ; amabilis

    ; change fadeout music to provided
;    org $7DEA
;    ; move.w #$21,d7 ; amabilis
;    move.w #$EC,d7 ; fade out

    ; remove stage clear demo
    org $3C2C
    ;move.w #$8,($FFFFF800).w
    ;rts
    jmp RepeatStage

    ; remove richy wait stage end
    org $219DC
    bra $219E2

    ; remove richy pause wait
    org $27380
    bra $2738A

; ---------------------------------------------------------------------------
; Change menu exit routine
; ---------------------------------------------------------------------------

    ; ingame
	org $6f4c
	jmp MenuExit
    bra $2962;$2950

; ---------------------------------------------------------------------------
; Turn off old routines
; ---------------------------------------------------------------------------

    org $6FD8
    jmp MenuNavigate

;    ; turn off cursor action
;    org $6FD8
;    bra $6FF0

;    ; cursor
;    org $6FF0
;    nop
;    nop

;    ; ctl
;    org $6FF4
;    nop
;    nop

;    ; bgm name
;    org $6FF8
;    bra $701C

;    ; bgm digit
;    org $701C
;    bra $702E

;    ; sfx digit
;    org $702E
;    bra $7040

;    ; pcm digit
;    org $7040
;    bra $7052

;    ; level name
;    org $7052
;    bra $7078

; ---------------------------------------------------------------------------
; Fix Controls position
; ---------------------------------------------------------------------------
CTLSPOS1    equ $48300003
CTLSPOS2    equ $48B00003
CTLSPOS3    equ $49300003

    org  $72ca
    dc.l CTLSPOS1
    org  $72dc
    dc.l CTLSPOS3
    org  $72ee
    dc.l CTLSPOS1
    org  $7300
    dc.l CTLSPOS2
    org  $7312
    dc.l CTLSPOS3
    org  $7324
    dc.l CTLSPOS1
    org  $7336
    dc.l CTLSPOS2
    org  $7348
    dc.l CTLSPOS3



; ---------------------------------------------------------------------------


 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

    include "mods/MainMenu/Functions.asm"

; ---------------------------------------------------------------------------
; VARIABLES
; ---------------------------------------------------------------------------

; menu navigation
MENUTYPE    equ $FFFFFD90 ;b
MENUTYPECPY equ $FFFFFD91 ;b
MENUCUR     equ $FFFFFD92 ;b
MT_MAIN     equ 0
MT_HELP     equ 1
MT_LEVSEL   equ 2
MT_SNDTST   equ 3

; level select
LEVSELCUR   equ $FFFFFD93 ;b
SNDTSTCUR   equ $FFFFFD94 ;b
HELPCUR     equ $FFFFFD95 ;b
HELPCURCPY  equ $FFFFFD96 ;b

; cursor movement
MENUCURMAX  equ $FFFFFD97 ;b
MENULINEPOS equ $FFFFFD98 ;w[2] - x,y
MENUCURRENT equ $FFFFFD9C ;l pointer to current cursor pos variable
MENUBTNCB   equ $FFFFFDA0 ;l callback - a,b,c,left,right
MENUALWCB   equ $FFFFFDA4 ;l callback - always executing
STAGESELTIMER   equ $FFFFFDA8 ;b stage select timer
STAGESELMID     equ 20
STAGESELMAX     equ 24

; ---------------------------------------------------------------------------
; Load game data and change to options
; ---------------------------------------------------------------------------

PermanentMenu:
    ; change game mode
    jsr     $2A32
    move.w  #$C,(GAMEMODE).w

    ; upload hexfont
    lea    fontHexGfx,a0
    lea    $C00000,a1
    move.l #$76000003,4(a1)
    move.w #(fontHexGfx_end-fontHexGfx)/4-1,d0
-:
    move.l (a0)+,(a1)
    dbf    d0,-

    ; reset features
    jsr    PauseMenuReset
    rts

; ---------------------------------------------------------------------------
; Execute while pause is active
; ---------------------------------------------------------------------------

MenuExit:
    ; old routine
    ;move.w  #4,($FFF800).l
    ;rts

    ; turn off pause
    move.w #1,($FFFFFD4A).w

    ; restart gamemode level
    jmp $2962


; ---------------------------------------------------------------------------
; Main Menu Prints
; ---------------------------------------------------------------------------

PrintMainMenu:
    move.b  #$ff,(MENUTYPECPY).w ; redraw menu
    jsr     PrintHeader
    ;jmp optionsInputPrint
    jmp     $7F30;$7EEC

; ---------------------------------------------------------------------------
; Main Menu Navigation
; ---------------------------------------------------------------------------

MenuNavigate:

    ; check if menu type changed
    move.b  (MENUTYPE).w,d0
    move.b  (MENUTYPECPY).w,d1
    cmp.b   d0,d1
    beq     PerformUI

    ; store as drawn
    move.b d0,(MENUTYPECPY).w

    ; redraw menu
    clr.w d0
    move.b (MENUTYPE).w,d0
    and.w #3,d0
    add.w d0,d0
    add.w d0,d0
    move.l ArrayMenuSetup(pc,d0.w),a6
    jsr (a6)
    bra PerformUI

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SHIFTMAIN   equ 10
SHIFTHELP   equ 6
SHIFTLEVSEL equ 4
SHIFTSNDTST equ 10

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ArrayMenuSetup:
    dc.l SetupMain
    dc.l SetupHelp
    dc.l SetupLevSel
    dc.l SetupSndTst
    dc.l 0

; ---------------------------------------------------------------------------

SetupMain:
    SetupUI SHIFTMAIN,10,4,MENUCUR,MenuMain,MenuPrintMain,0

SetupHelp:
    SetupUI SHIFTHELP,10,3,HELPCUR,MenuHelp,MenuPrintHelp,MenuHelpAlw

SetupLevSel:
    SetupUI SHIFTLEVSEL,10,0,LEVSELCUR,MenuLevSel,MenuPrintLevSel,subMenuLevSelCursor

SetupSndTst:
    SetupUI SHIFTSNDTST,10,3,SNDTSTCUR,MenuSndTst,MenuPrintSndTst,0

; ---------------------------------------------------------------------------
; MAIN
; d5[in] - pad presses

MMHELP equ 0
MMLEVS equ 1
MMSNDT equ 2
MMCTLS equ 3

MenuMain:
    move.l  (MENUCURRENT).w,a0
    move.b  (a0),d1
    
    ; left right
    move.b  d5,d0
    and.b   #(BTNLEFT|BTNRIGHT),d0
    beq     mmSkipLR

    ; check current cursor is CTL
    cmp.b   #MMCTLS,d1
    bne     mmSkipLR

    ; #################################

    btst    #BRIGHT,d5
    beq     +
    jsr     $7232 ; right

+:
    btst    #BLEFT,d5
    beq     +
    jsr     $724A ; left

+:
    ;btst    #BC,d5
    ;beq     +
    ;jsr     $739E ; c

;+:

     ; #################################

   ; print ctls
    jsr $725C 

mmSkipLR:

    ; A,B,C
    move.b  d5,d0
    and.b   #(BTNA|BTNB|BTNC),d0
    beq     mmSkipABC

    ; change modes

    cmp.b   #MMHELP,d1
    bne     +
    move.b  #MT_HELP,(MENUTYPE).w
    clr.b   (PAD1PR).w
    bra     mmSkipABC

+:
    cmp.b   #MMLEVS,d1
    bne     +
    move.b  #MT_LEVSEL,(MENUTYPE).w
    clr.b   (PAD1PR).w
    clr.b   (PAD1PR).w
    bra     mmSkipABC

+:
    cmp.b   #MMSNDT,d1
    bne     +
    move.b  #MT_SNDTST,(MENUTYPE).w
    clr.b   (PAD1PR).w
    bra     mmSkipABC

+:

mmSkipABC:

    rts

; ---------------------------------------------------------------------------
; HELP
; d5[in] - pad presses

MenuHelpAlw:

    move.b  (HELPCUR).w,d0
    move.b  (HELPCURCPY).w,d1
    cmp.b   d0,d1
    beq     + ; do not reprint

    move.b  d0,(HELPCURCPY).w
    jsr     ReprintHelpText

+:
    rts

 MenuHelp:

    ; A,B,C
    move.b  d5,d0
    and.b   #(BTNA|BTNB|BTNC),d0
    beq     mhSkipABC

    move.b  #MT_MAIN,(MENUTYPE).w
    bra     mhSkipABC

mhSkipABC:
    rts

; ---------------------------------------------------------------------------
; LEVSEL
; d5[in] - pad presses

MenuLevSel:
    ; A,B,C
    move.b  (PAD1).w,d5
    move.b  d5,d0
    and.b   #(BTNA|BTNB|BTNC),d0
    beq     mlsSkipABC

    move.b  #MT_MAIN,(MENUTYPE).w
    bra     mlsSkipABC

mlsSkipABC:

    rts

mlsSkipUD:
    clr.w   (STAGESELTIMER).w
    rts

subStageSelUpDown:
    move.b  (PAD1).w,d5

    ; max stages
    jsr    subCountLevels

    ; check if up or down
    and.b   #(BTNUP|BTNDOWN),d5
    beq     mlsSkipUD

    ; timer
    jsr     subTimerUD

    ; check up down
    btst    #BUP,d5
    beq     +

    jsr     $7440
    ;jsr     subLevelsDraw
    rts
+:
    ; check up down
    btst    #BDOWN,d5
    beq     +

    jsr     $7422
    ;jsr     subLevelsDraw
    rts
+:


subTimerUD:
    ; get timer
    move.b  (STAGESELTIMER).w,d2

    ; check if 0
    tst.b   d2
    beq     studIncr

    ; check if mid
    cmp.b   #STAGESELMID,d2
    beq     studIncr

    ; check if not max
    cmp.b   #STAGESELMAX,d2
    beq     studMid

    ; clear btns
    clr.b   d5

studIncr:
    addq.b  #1,(STAGESELTIMER).w
    rts

studMid:
    move.b  #STAGESELMID,(STAGESELTIMER).w
    rts

subMenuLevSelCursor:
    ; write pos
    move.l #$47060003,$C00004
    jsr    uiPrintCurEx

    jsr    subLevelsDraw
    rts

; used to draw all levels line by line
subLevelsDraw:

    ; eval up down presses
    jsr    subStageSelUpDown

    ; max stages
    jsr    subCountLevels

    ; current stage line
    clr.w  d5
    move.b (CHEATSTAGEVAL).w,d5

    ; calc magic
    sub.w  #6,d5
    bgt    +
    add.w  d1,d5
+:

    ; max lines on screen
    move.w #10,d4

    ; level names
    lea    $7452,a0

    ; calc magic 2
    lsl.w  #2,d5
    add.l  d5,a0

    ; vdp data
    lea    $C00000,a5

    ; coords
    move.w #$5,d0
    move.w #$9,d1

; draw every line
sldDrawLineLoop:

    ; get text
    move.l (a0)+,a3

    ; if zero - loop
    cmp.l  #0,a3
    bne    +
    lea    $7452,a0
    move.l (a0)+,a3
+:

    ; skip vdp command
    addq.l #4,a3

    movem.w d0-d1,-(sp)
    jsr    uiCalcCursosPos
    movem.w (sp)+,d0-d1

    ; print empty
    move.l d2,$C00004
    lea    $7572,a6
    jsr    RomPrintString

    ; print stage name
    move.l d2,$C00004
    move.l a3,a6
    jsr    RomPrintString

    addq.w #1,d1
    dbra   d4,sldDrawLineLoop
    rts


; count maximum lines
subCountLevels:
;    lea    $7452,a0
;    clr.w  d1
;-:
;    move.l (a0)+,d0
;    beq    +
;    addq.w #1,d1
;    bra    -
;+:
    move.w  #$46,d1
    rts

; ---------------------------------------------------------------------------
; SNDTST
; d5[in] - pad presses

MST_BGM     equ 0
MST_SE      equ 1
MST_PCM     equ 2

MenuSndTst:

    ; A,B,C
    move.b  d5,d0
    and.b   #(BTNC),d0
    beq     mstSkipABC

    move.b  #MT_MAIN,(MENUTYPE).w
    bra     mstSkipABC

mstSkipABC:
    move.l  (MENUCURRENT).w,a0
    move.b  (a0),d1

    cmp.b   #MST_BGM,d1
    bne     mstNoBgm

    btst    #BRIGHT,d5
    beq     +
    jsr     $739E
    bra     mpstDigits
+:
    btst    #BLEFT,d5
    beq     +
    jsr     $73B8
    bra     mpstDigits
+:
    btst    #BA,d5
    beq     +
    jsr     $735A
    bra     mpstDigits
+:
    btst    #BB,d5
    beq     +
    jsr     $736E
    bra     mpstDigits
+:
    rts

mstNoBgm:
    
    cmp.b   #MST_SE,d1
    bne     mstNoSe

    btst    #BRIGHT,d5
    beq     +
    jsr     $73DC
    bra     mpstDigits
+:
    btst    #BLEFT,d5
    beq     +
    jsr     $73EA
    bra     mpstDigits
+:
    btst    #BA,d5
    beq     +
    jsr     $735A
    bra     mpstDigits
+:
    btst    #BB,d5
    beq     +
    jsr     $73CE
    bra     mpstDigits
+:
    rts
    
mstNoSe:
    
    cmp.b   #MST_PCM,d1
    bne     mstNoPcm

    btst    #BRIGHT,d5
    beq     +
    jsr     $7406
    bra     mpstDigits
+:
    btst    #BLEFT,d5
    beq     +
    jsr     $7414
    bra     mpstDigits
+:
    btst    #BA,d5
    beq     +
    jsr     $735A
    bra     mpstDigits
+:
    btst    #BB,d5
    beq     +
    jsr     $73F8
    bra     mpstDigits
+:
    rts
    
mstNoPcm:
    

    rts

; ---------------------------------------------------------------------------

MenuPrintMain:
    jsr ClearScreen
    PrintString SHIFTMAIN,10,textMainHelp
    PrintString SHIFTMAIN,12,textMainLevSel
    PrintString SHIFTMAIN,14,textMainSndTst
    PrintString SHIFTMAIN,16,textMainCtl

    PrintString 3,20,textMainCtls1
    PrintString 3,21,textMainCtls2
    PrintString 3,22,textMainCtls3
    PrintString 24,22,textVersion
    jsr $725C ; print ctls
    rts

textMainHelp:   dc.b "[+] HELP",0
textMainLevSel: dc.b "[+] STAGE SELECT",0
textMainSndTst: dc.b "[+] SOUND TEST",0
textMainCtl:    dc.b "< > CONTROLS:",0
textMainCtls1:  dc.b " A,B,C  - ENTER",0
textMainCtls2:  dc.b "  < >   - CHANGE",0
textMainCtls3:  dc.b " START  - BEGIN",0
    align 2

MenuPrintHelp:
    jsr ClearScreen

    ; text
    clr.b (HELPCURCPY).w
    jsr ReprintHelpText

    ; ctl
    PrintString 3,20,textHelpCtls1
    PrintString 3,21,textHelpCtls2
    PrintString 3,22,textHelpCtls3
    rts

ReprintHelpText:
    jsr     ClearTextField

 ; digits
    PrintString SHIFTHELP-1,10,textHelpLetter1
    PrintString SHIFTHELP-1,12,textHelpLetter2
    PrintString SHIFTHELP-1,14,textHelpLetter3

    move.b  (HELPCUR).w,d0
    and.w   #3,d0
    add.w   d0,d0
    add.w   d0,d0
    move.l  arrayHelpPages(pc,d0.w),a6
    move.l  #SHIFTHELP-2,d0
    move.w  #10,d1
    jsr     uiPrintStringEx
    rts

textHelpLetter1:  dc.b "1:",0
textHelpLetter2:  dc.b "2:",0
textHelpLetter3:  dc.b "3:",0
textHelpCtls1:  dc.b " A,B,C  - RETURN",0
textHelpCtls2:  dc.b "UP,DOWN - SCROLL",0
textHelpCtls3:  dc.b " START  - BEGIN",0
    align 2

arrayHelpPages:
    dc.l textHelpPage1
    dc.l textHelpPage2
    dc.l textHelpPage3
    dc.l 0

textHelpPage1:  dc.b " >THIS HACK IS MADE FOR THOSE WHO",$d
                dc.b " WANTS TO SPEEDRUN PULSEMAN.",$d
                dc.b " USING THIS HACK YOU CAN PRACTICE",$d
                dc.b " IN MORE EASE WAY.",$d
                dc.b ">BE SURE TO SELECT STAGE AND",$d
                dc.b " START GAME.",0
textHelpPage2:  dc.b " >WHILE IN THE GAME GO INTO PAUSE.",$d
                dc.b " INSIDE PAUSE YOU WILL SEE MENU.",$d
                dc.b " USE IT TO ADJUST ANYTHING",$d
                dc.b " YOU NEED.",$d
                dc.b ">USE BUTTONS TO EXECUTE",$d
                dc.b " MENU ITEMS.",$d
                dc.b ">SOME ITEMS (LIKE EDIT) CAN BE",$d
                dc.b " OPENED AS SUBMENU.",0
textHelpPage3:  dc.b " >YOU CAN TRAVEL THRU STAGE USING",$d
                dc.b " FEATURE CALLED 'MOVE PLAYER'.",$d
                dc.b ">WHILE 'MOVE' IS ACTIVATED,",$d
                dc.b " YOU'RE ALLOWED TO TRAVEL ACROSS",$d
                dc.b " WHOLE STAGE.",$d
                dc.b ">IF YOU GOT STUCK, HOLD 'START'",$d
                dc.b " FOR 3 SECONDS.",$d
                dc.b " GAME WILL RESET AND",$d
                dc.b " RETURN TO MENU.",0
    align 2

MenuPrintLevSel:
    jsr ClearScreen

    ; write pos
    move.l #$47060003,$C00004
    jsr   uiPrintCurEx

    ; ctl
    PrintString 3,21,textHelpCtls1
    PrintString 3,22,textHelpCtls3
    rts

MenuPrintSndTst:
    jsr ClearScreen

    ; BGM, SE, VOICE
    PrintString 10,10,$7FDE
    PrintString 10,12,$7FE6
    PrintString 10,14,$7FEE

    ; cls
    PrintString 3,18,textSndCtlsA
    PrintString 3,19,textSndCtlsB
    PrintString 3,20,textSndCtlsC
    PrintString 3,21,textSndCtlsLR
    PrintString 3,22,textSndCtlsS

    ; DIGITS
mpstDigits:

     lea     ($C00000).l,a5
   ; print empty
    move.l  #$452C0003,$C00004
    lea     $7ADE,a6
    jsr     RomPrintString

     ; get bgm value
    move.b  ($FFFD2C).l,d0
    asl.w   #2,d0

   ; print bgm name
    lea     $7AF0,a0
    movea.l (a0,d0.w),a6
    addq.l  #4,a6
    move.l  #$452C0003,$C00004
    jsr     RomPrintString

    ; get bgm value
    move.b  ($FFFD2C).l,d0
    addq.b  #1,d0
    move.l  #$45260003,d1
    jsr     RomPrintDigit

    ; get se value
    move.b  ($FFFD2D).l,d0
    addq.b  #1,d0
    move.l  #$46260003,d1
    jsr     RomPrintDigit

    ; get voice value
    move.b  ($FFFD2E).l,d0
    addq.b  #1,d0
    move.l  #$47260003,d1
    jsr     RomPrintDigit

    rts

textSndCtlsA:  dc.b "   A    - STOP",0
textSndCtlsB:  dc.b "   B    - PLAY",0
textSndCtlsC:  dc.b "   C    - RETURN",0
textSndCtlsLR: dc.b "  < >   - CHANGE",0
textSndCtlsS:  dc.b " START  - BEGIN",0

; ---------------------------------------------------------------------------
; TEXTS
; ---------------------------------------------------------------------------

PrintHeader:
    PrintString 3,5,textWelcome
    PrintString 2,7,textSegaman
    rts

ClearTextField:
    move.w  #9,d1
    bra ClearScreenFunc

ClearScreen:
    move.w  #13,d1

ClearScreenFunc:
    lea     $C00000,a5
    move.l  #((((2*2)+(8*64*2))<<16)|($40000003)),d0
    move.l  d0,4(a5)
    lea     textClear,a6
    jsr     RomPrintString

csLoop:
    add.l   #$800000,d0
    move.l  d0,4(a5)
    lea     textClear,a6
    jsr     RomPrintString

    dbra    d1,csLoop

    rts

; ---------------------------------------------------------------------------

textClear:   dc.b "                                    ",0
textWelcome: dc.b $22,"PULSEMAN SPEEDRUN PRACTICE HACK",$22,0
textSegaman: dc.b "@2022 SEGAMAN > HTTPS://SEGAMAN.TOP/",0
textVersion: dc.b "VERSION 3",0
    align 2

; ---------------------------------------------------------------------------

; hex font gfx
fontHexGfx:
    binclude    "mods/MainMenu/HexFont.gfx"
fontHexGfx_end:


; ---------------------------------------------------------------------------


 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
