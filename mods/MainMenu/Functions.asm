; MACROS

SetupUI macro curx,cury,curmax,current,callback,reprint,always
    move.w #curx,(MENULINEPOS+0).w
    move.w #cury,(MENULINEPOS+2).w
    move.b #curmax,(MENUCURMAX).w ; if max = 0, always will execute callback
    move.l #current,(MENUCURRENT).w
    move.l #callback,(MENUBTNCB).w
    move.l #always,(MENUALWCB).w
    jsr    reprint
    rts
    endm

; ---------------------------------------------------------------------------
; Perform UI
; ---------------------------------------------------------------------------
PerformUI:
    move.l  MENUALWCB,a0
    cmp.l   #0,a0
    beq     +
    jsr     (a0)
+:

    move.b  (PAD1PR).w,d5

    ; up down
    btst    #BUP,d5
    bne.w   uiBtnUp

    btst    #BDOWN,d5
    bne.w   uiBtnDown

    ; left,right
    ; a,b,c
    and.w   #(BTNLEFT|BTNRIGHT|BTNA|BTNB|BTNC),d5
    bne.w   uiBtnCallback

    move.b MENUCURMAX,d5
    beq    +
    jsr    uiPrintCur
+:
    rts
   
; ---------------------------------------------------------------------------
; Get cursor position
; d0[use] - x
; d1[out] - GPU Address Request
; a6[use] - current cursor pos
; ---------------------------------------------------------------------------
uiGetCursorPos:
    move.w #-2,d0
    add.w  MENULINEPOS,d0 ; x
    move.w MENULINEPOS+2,d1 ; y
    move.l MENUCURRENT,a3	; current cursor pos
    add.b  (a3),d1
    add.b  (a3),d1
    rts

; ---------------------------------------------------------------------------
; Calc cursor pos
; d0[in] - x
; d1[in] - y
; used: d2,d3
; ---------------------------------------------------------------------------
uiCalcCursosPos:

    ; calc pos
    move.l #$40000003,d2
    add.w  d0,d0
    move.w #(64*2),d3
    mulu.w d3,d1
    or.w   d1,d0
    and.l  #$3FFF,d0
    swap   d0
    add.l  d0,d2
    rts

; ---------------------------------------------------------------------------
; Print cursor
; ---------------------------------------------------------------------------
uiPrintCur:
    jsr    uiGetCursorPos
    jsr    uiCalcCursosPos

    ; write pos
    move.l d2,$C00004

uiPrintCurEx:

    ; get cursor text
    lea    uiStringCursor,a6

    ; check if need to empty
    btst   #3,($FFFFF805).w
    bne    uiCpNotHide

    ; replace with empty
    lea    uiStringClear,a6
uiCpNotHide:

    ; print text
    lea    $C00000,a5
    jmp    RomPrintString

; ---------------------------------------------------------------------------
; Free cursor place
; ---------------------------------------------------------------------------
uiReprintCursor:
    jsr    uiGetCursorPos
    jsr    uiCalcCursosPos

    ; write pos
    move.l d2,$C00004

    ; print empty
    lea    uiStringClear,a6

    lea    $C00000,a5
    jmp    RomPrintString

; ---------------------------------------------------------------------------
; Cursor
; ---------------------------------------------------------------------------
uiStringCursor: dc.b "#",0
uiStringClear:  dc.b " ",0

; ---------------------------------------------------------------------------
; Buttons
; ---------------------------------------------------------------------------

uiBtnDown:
    move.b MENUCURMAX,d5
    beq    uiBtnCallback

    jsr    uiReprintCursor

    add.b  #1,(a3)
    move.b (a3),d0

    cmp.b  d5,d0
    bne    uiPrintCur
    clr.b  (a3)
    bra    uiPrintCur

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

uiBtnUp:
    move.b MENUCURMAX,d5
    beq    uiBtnCallback
    sub.b  #1,d5

    jsr    uiReprintCursor

    sub.b  #1,(a3)
    move.b (a3),d0

    cmp.b  #-1,d0
    bne    uiPrintCur
    move.b d5,(a3)
    bra    uiPrintCur

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

uiBtnCallback:
    move.l MENUBTNCB,a0
    jsr    (a0)
    rts

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

uiPrintStringEx:

    jsr     uiCalcCursosPos
    lea     ($C00000).l,a5
    move.l  d2,4(a5)
    move.w  ($FFF822).l,d7 ; base char

loc_0_1FF8:
    moveq   #0,d6
    move.b  (a6)+,d6
    beq.s   locret_0_2008

    cmp.b   #$d,d6
    bne     +
    add.l   #$800000,d2
    move.l  d2,4(a5)

+:
    add.w   d7,d6
    ori.w   #$8000,d6
    move.w  d6,(a5)
    bra.s   loc_0_1FF8

locret_0_2008:
    rts
