;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; Change title copyright text
; ---------------------------------------------------------------------------

    ; replace score print routine
	org $8774
	jmp ShowCoordinates

; ---------------------------------------------------------------------------


 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; VARS
; ---------------------------------------------------------------------------

PLAYERX equ $FFFFE020
PLAYERY equ $FFFFE024

; ---------------------------------------------------------------------------
; Execute while pause is active
; ---------------------------------------------------------------------------

ShowCoordinates:
    ; setup values
    move.w  (CAMERAX).w,d2
    add.w   (PLAYERX).w,d2
    swap    d2
    move.w  (CAMERAY).w,d2
    add.w   (PLAYERY).w,d2

    move.l  #$15000A0,d1
    move.w  #7,d0

scLoop:

    ; roll left, store and get first 4 nybbles
    rol.l   #4,d2
    move.l  d2,d4
    andi.l  #$F,d2

    ; print sprite
    moveq   #0,d3
    add.w   #$7B0,d2
    jsr     $87C8

    ; loop
    move.l  d4,d2
    dbf     d0,scLoop
    jmp     $87AA
    ;rts

; ---------------------------------------------------------------------------

 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
