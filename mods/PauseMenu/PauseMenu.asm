;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

    include "mods/PauseMenu/MovePlayer.asm"

; ---------------------------------------------------------------------------
; Change title copyright text
; ---------------------------------------------------------------------------

    ; ingame
	org $272D6
	jmp PauseMenuInGame

    ;org $4ec46
    ;dc.l PauseMenuInPause
    org $273C4
    jmp PauseMenuInPause

    ; repeat stage
    org $230BE
    jmp RepeatStage

; ---------------------------------------------------------------------------


 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

    include "mods/PauseMenu/MovePlayer.asm"

; ---------------------------------------------------------------------------
; VARIABLES
; ---------------------------------------------------------------------------

; sub menu variable
PMSUB       equ $FFFFFD80
PMREPEAT    equ $FFFFFD81
PMRESTART   equ $FFFFFD82
PMINFVOLT   equ $FFFFFD83
PMREPEATSTAGE equ $FFFFFD84
PMINFLIFE   equ OCINV;$FFFFFD45
PMBTNPRESS  equ $FFFFF810

; function to print text
PRINTSTRING equ $1FE8

; enum sub menu type
SMT_MAIN    equ 0
SMT_EDIT    equ 1

; enum menu buttons
SMB_UP      equ $01
SMB_DOWN    equ $02
SMB_LEFT    equ $04
SMB_RIGHT   equ $08
SMB_B       equ $10
SMB_C       equ $20
SMB_A       equ $40
;SMB_START   equ $80

; ---------------------------------------------------------------------------
; Execute while pause is active
; ---------------------------------------------------------------------------

PauseMenuInPause:
    ; execute interraction
    jsr     PauseMenuInterract

    ; check if unpause
    
    btst    #5,($FFFA29).l
    bne.w   $27314
    btst    #7,($FFF810).l
    bne.w   DoIt;$272E2

    ; hide window
    ;move.w  #$9200,$C00004

    bra     $273DC

DoIt:
    ; hide window
    ;bclr    #4,($FFFFFB73).w
    ;bset    #0,($FFFFFB6A).w
    jsr     $1978 ; turn off semifade
    move.w  #$9200,$C00004
    bra     $272E2

PauseMenuReset:
    clr.l   (PMSUB).w
    clr.b   (PMINFLIFE).w
    rts

; ---------------------------------------------------------------------------
; Execute while pause is inactive
; ---------------------------------------------------------------------------

PauseMenuInGame:
    ; check if pause activated
    btst    #7,($FFFFF810).w
    bne     ActivatePauseMenu;$2734E

    jsr     LevelRestarter
    bra     $27314

; ---------------------------------------------------------------------------
; Activate pause menu
; ---------------------------------------------------------------------------

ActivatePauseMenu:

    ; reset variables
    move.b  #0,(PMSUB).w
    move.b  #$ff,(PMRESTART).w

    ; show window
    move.w  #$9297,$C00004

    ; save a0 into stack
    move.l  a0,-(sp)

    ; print buttons
    ;jsr     PrintButtons

    ; print menu items
    ;lea     MenuItems,a0
    ;jsr     PrintTextList

    ; print menu items
    ;lea     EditItems,a0
    ;jsr     PrintTextList

    jsr ReprintMenu

    ; return a0 from stack
    move.l  (sp)+,a0

    ; execute source code
    btst    #1,($FFFFFB6D).w
    bne.s   boi_27314
    bra     $2734E

boi_27314:
    bra    $27314

; ---------------------------------------------------------------------------
; Level restarter
; ---------------------------------------------------------------------------

LevelRestarter:
    cmp.b #$ff,(PMRESTART).w
    beq   lrLocret

    subq.b #1,(PMRESTART).w
    bne   lrLocret

    jsr $1F142
    ;bset    #7,($FFF810).l

lrLocret:
    rts

; ---------------------------------------------------------------------------
; Repeat stage
; ---------------------------------------------------------------------------

RepeatStage:

    ; check stage
    tst.b  (PMREPEAT).w
    bne    +
    addq.w  #1,($FFFD4A).l
+:

    bsr     $2310E
    jmp     $230C6


; ---------------------------------------------------------------------------
; Interact with player
; ---------------------------------------------------------------------------

PauseMenuInterract:
    move.b (PMBTNPRESS).w,d0
    cmp.b  #SMB_UP,d0
    beq    pmiUp
    cmp.b  #SMB_DOWN,d0
    beq    pmiDown
    cmp.b  #SMB_LEFT,d0
    beq    pmiLeft
    cmp.b  #SMB_RIGHT,d0
    beq    pmiRight
    cmp.b  #SMB_B,d0
    beq    pmiB
    cmp.b  #SMB_C,d0
    beq    pmiC
    cmp.b  #SMB_A,d0
    beq    pmiA
    rts

pmiUp:
    move.w #0,d0
    bra    pmiPerform
pmiDown:
    move.w #4,d0
    bra    pmiPerform
pmiLeft:
    move.w #8,d0
    bra    pmiPerform
pmiRight:
    move.w #12,d0
    bra    pmiPerform
pmiB:
    move.w #16,d0
    bra    pmiPerform
pmiC:
    move.w #20,d0
    bra    pmiPerform
pmiA:
    move.w #24,d0
    bra    pmiPerform

pmiPerform:
    ; read link to sub links
    lea     MenuSubrotines,a0
    move.b  (PMSUB).w,d1
    and.w   #$f,d1
    add.w   d1,d1
    add.w   d1,d1
    move.l  (a0,d1.w),a0

    move.l  (a0,d0.w),a0
    cmp.l   #0,a0
    beq     pmippLocret
    jsr     (a0)

pmippLocret:
    rts

; ---------------------------------------------------------------------------
; Reprint menu items
; ---------------------------------------------------------------------------

ReprintMenu:
    bset    #4,($FFFFFB73).w ; turn on semi-fade
    jsr     ClearWindow
    lea     MenuLinks,a0
    move.b  (PMSUB).w,d0
    and.w   #$f,d0
    add.w   d0,d0
    add.w   d0,d0
    move.l  (a0,d0.w),a0
    jsr     PrintTextList
    bra     PrintVariables

; ---------------------------------------------------------------------------
; Clear window
; ---------------------------------------------------------------------------
ClearWindow:
    lea    $C00000,a0
    move.l #$7B800003,4(a0)
    move.w #32*5,d0
    move.l #0,d1

cwLoop:
    move.l d1,(a0)
    dbra   d0,cwLoop
    rts

; ---------------------------------------------------------------------------
; Print buttons
; ---------------------------------------------------------------------------

;PrintButtons:

;    lea     txtButtons,a6
;apmLoop:
;    cmp.l   #0,(a6)
;    beq     apmDone
;    jsr     PRINTSTRING
;    bra     apmLoop

;apmDone:
;    rts

; ---------------------------------------------------------------------------
; Print Text List
; a0[in] - pointer to list ending with (0).l
; ---------------------------------------------------------------------------
PrintTextList:
    ;lea     MenuItems,a0
apmLoop2:
    move.l  (a0)+,a6
    cmp.l   #0,a6
    beq     apmDone2
    jsr     PRINTSTRING
    bra     apmLoop2
apmDone2:
   rts

; ---------------------------------------------------------------------------
; Print variables
; ---------------------------------------------------------------------------

txtTrue:
    dc.l $7B880003
    dc.b "1",0
    align 2

txtFalse:
    dc.l $7B880003
    dc.b "0",0
    align 2

PrintVariables:

    ; only for submenu
    cmp.b #SMT_EDIT,(PMSUB).w
    beq   +
    rts

+:

    ;reserve stack
    subq.l #6,sp

    ; print teleport
    move.l  #$7C220003,(sp)
    move.w  #$3000,4(sp)

    cmp.w   #$8044,($FFFFE000).w
    bne     +
    add.b   #1,4(sp)
+:
    jsr     pvPrint

    ; print inf volt
    move.l  #$7CA20003,(sp)
    move.w  #$3000,4(sp)

    tst.b  (PMINFVOLT).w;btst    #7,($FFFFFA31).w
    beq     +
    add.b   #1,4(sp)
+:
    jsr     pvPrint

    ; print inf life
    move.l  #$7D220003,(sp)
    move.w  #$3000,4(sp)

    tst.b  (PMINFLIFE).w
    beq     +
    add.b   #1,4(sp)
+:
    jsr     pvPrint

    ; print repeat stage
    move.l  #$7BCC0003,(sp)
    move.w  #$3000,4(sp)

    tst.b  (PMREPEAT).w
    beq     +
    add.b   #1,4(sp)
+:
    jsr     pvPrint

    ; end
    addq.l  #6,sp
    rts

pvPrint:

    lea     4(sp),a6
    jmp     PRINTSTRING

; ---------------------------------------------------------------------------
; Menu subrotines links
; ---------------------------------------------------------------------------

MenuSubrotines:
    dc.l msubMain
    dc.l msubEdit
    dc.l 0

msubMain:
    dc.l msmDarkness
    dc.l 0
    dc.l 0
    dc.l 0
    dc.l msmEdit
    dc.l msmExit
    dc.l msmRestart

msubEdit:
    dc.l mseHeal
    dc.l mseMove
    dc.l mseInfVolt
    dc.l mseInfLife
    dc.l 0;mseKill
    dc.l mseReturn
    dc.l msmRepeat

; ---------------------------------------------------------------------------
; Menu subrotines
; ---------------------------------------------------------------------------


; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; MAIN

msmEdit:
    jsr    ClearWindow
    move.b #SMT_EDIT,(PMSUB).w
    jsr    ReprintMenu
    rts

msmExit:
    ; reset game
    move.l  $4,a0
    jmp     (a0)

;    ; setup game mode switch
;    lea     $FFFB07,a0
;    move.b  #$80,(PAD1PR).w

;    ; routines to fade screen
;    jsr     ($8664).l
;    jsr     ($1146).w

;    jsr     ($29A0).w ; restart


;    ; change game mode
;    move.w  #$C,(GAMEMODE).w
;    rts

msmRestart:

    ; store level
    move.w  (CURRENTLEVEL).w,d0 
    move.w  d0,-(sp)

    ; setup game mode switch
    lea     $FFFB07,a0
    move.b  #$80,(PAD1PR).w

    ; routines to fade screen
    jsr     ($8664).l
    jsr     ($1146).w

    jsr     ($29A0).w ; restart

    ; restore level
    move.w  (sp)+,d0
    move.w  d0,(CURRENTLEVEL).w 

    rts

msmDarkness:
    jmp $1146

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; EDIT

mseHeal:
    ;move.b #3,($FFFFFD46).w

    moveq #3,d0
    jsr $2115C
    jsr $DDFC
    rts

mseMove:
    move.w #$8044,($FFFFE000).w
    jsr    PrintVariables
    rts

mseInfVolt: ; found in $1B380
    ;btst   #7,($FFFFFA31).w
    tst.b  (PMINFVOLT).w
    beq    +
    move.b #0,(PMINFVOLT).w
    bclr   #7,($FFFFFA31).w
    jsr    PrintVariables
    rts
+
    move.b #1,(PMINFVOLT).w
    bset   #7,($FFFFFA31).w
    jsr    PrintVariables
    rts;

mseInfLife:
    jsr    mseHeal(pc)
    tst.b  (PMINFLIFE).w
    bne    +
    move.b #1,(PMINFLIFE).w
    jsr    PrintVariables
    rts

+:
    clr.b (PMINFLIFE).w
    move.b #3,($FFFFFD46).w
    jsr    PrintVariables
    rts

mseKill:
    ;bset    #7,($FFF810).l
    ;move.b  #0,($FFFFFD46).w
    ;move.b #$2,($FFFFCB1C).w


    ;bset    #5,($FFFA29).l
    ;bset    #7,($FFF810).l

    rts

mseReturn:
    jsr    ClearWindow
    move.b #SMT_MAIN,(PMSUB).w
    jsr    ReprintMenu
    rts

msmRepeat:
    tst.b  (PMREPEAT).w
    bne    +
    move.b #1,(PMREPEAT).w
    jsr    PrintVariables
    rts

+:
    clr.b (PMREPEAT).w
    jsr    PrintVariables
    rts

; ---------------------------------------------------------------------------
; Menu texts
; ---------------------------------------------------------------------------

MenuLinks:
    dc.l MenuItems
    dc.l EditItems
    dc.l 0

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; buttons
;txtButtons:
;    dc.b $7C,$04,$00,$03,"A",0
;    dc.b $7C,$84,$00,$03,"B",0
;    dc.b $7D,$04,$00,$03,"C",0
;    dc.l 0

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; main menu
MenuItems:
    dc.l txtRestartLevel
    dc.l txtEdit
    dc.l txtExit
    dc.l 0

txtRestartLevel:
    dc.l $7B880003
    dc.b "A * RESTART STAGE    ",0
    align 2

txtEdit:
    dc.l $7C080003
    dc.b "B * OPEN INGAME MENU ",0
    align 2

txtExit:
    dc.l $7C880003
    dc.b "C * BACK TO MAIN MENU",0
    align 2

; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; submenu EDIT
EditItems:
    ; up
    dc.l txtHealPlayer
    ; down
    dc.l txtMovePlayer
    ; left
    dc.l txtInfiniteVolt
    ; right
    dc.l txtInvulnerability
    ; a
    dc.l txtRepeatLevel
    ; c
    dc.l txtReturn

    dc.l 0

txtHealPlayer:
    dc.l $7B840003
    dc.b "UP  * RESTORE HP   ",0
    align 2

txtMovePlayer:
    dc.l $7C040003
    dc.b "DWN * NAVIGATE     ",0
    align 2

txtInfiniteVolt:
    dc.l $7C840003
    dc.b "LFT * INF VOLT     ",0
    align 2

txtInvulnerability:
    dc.l $7D040003
    dc.b "RGT * INF LIFE     ",0
    align 2

txtRepeatLevel:
    dc.l $7BAA0003
    dc.b "A * REPEAT STAGE   ",0
    align 2

txtReturn:
    dc.l $7CAA0003
    dc.b "C * BACK TO MENU   ",0
    align 2


; ---------------------------------------------------------------------------

 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
