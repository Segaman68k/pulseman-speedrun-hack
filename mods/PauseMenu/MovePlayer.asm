;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; Store pointer to this object
; ---------------------------------------------------------------------------

    ; ingame
	org $4ecea
	dc.l MovePlayerObject

; ---------------------------------------------------------------------------


 endif

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; VARIABLES
; ---------------------------------------------------------------------------

MPBTN  equ $FFFFF80E
MPB_UP		equ 0
MPB_DOWN	equ 1
MPB_LEFT	equ 2
MPB_RIGHT	equ 3
MPB_B		equ 4
MPB_C		equ 5
MPB_A		equ 6
MPB_START	equ 7

MPB_SPEED	equ $80000
OBJECTX		equ $20
OBJECTY		equ $24
OBJECTH		equ $28
OBJECTV		equ $2C

; ---------------------------------------------------------------------------
; Object
; ---------------------------------------------------------------------------
MovePlayerObject:
	;cmp.l  #$FFE000,a0
	;bne    mpoIgnore
	
	clr.l  OBJECTH(a0)
	clr.l  OBJECTV(a0)
	move.b  (MPBTN).w,d0
	btst	#MPB_UP,d0
	beq		mpoNoUp
	move.l  #-MPB_SPEED,OBJECTV(a0)
mpoNoUp:
	btst	#MPB_DOWN,d0
	beq		mpoNoDown
	move.l  #MPB_SPEED,OBJECTV(a0)
mpoNoDown:
	btst	#MPB_LEFT,d0
	beq		mpoNoLeft
	move.l  #-MPB_SPEED,OBJECTH(a0)
	bclr    #7,2(a0)
mpoNoLeft:
	btst	#MPB_RIGHT,d0
	beq		mpoNoRight
	move.l  #MPB_SPEED,OBJECTH(a0)
	bset    #7,2(a0)
mpoNoRight:
	btst	#MPB_A,d0
	beq		mpoNoA
	move.w	#$8004,(a0)
mpoNoA:

mpoIgnore:
	rts
; ---------------------------------------------------------------------------

 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;