;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0


; ---------------------------------------------------------------------------
; Enable cheats via jump
; ---------------------------------------------------------------------------

	org $1e74
	;jsr OcCheats
	;rts
	jmp OcCheats

; ---------------------------------------------------------------------------
; Change title copyright text
; ---------------------------------------------------------------------------

	org $2c1c
	dc.l copyrightText

; ---------------------------------------------------------------------------
; Add options item max
; ---------------------------------------------------------------------------

	org $7142
	dc.w 6

	org $714c
	dc.w 7

; ---------------------------------------------------------------------------
; Menu cursor positions
; ---------------------------------------------------------------------------

	org $71ba
	dc.l optionsCursor2,optionsCursor1
	org $71ea
	dc.l optionsCursor4,optionsCursor3

; ---------------------------------------------------------------------------
; Options hack
; ---------------------------------------------------------------------------
	org $7082
	jmp    OptionsPrints

	org $7ee2
 if modPauseMenu=0
	jmp    optionsInputPrint
 endif
 if modPauseMenu=1
	jmp    PrintMainMenu
 endif

; ---------------------------------------------------------------------------
; Change Life Meter Color Table
; ---------------------------------------------------------------------------
	org $de06
	lea     (LifeMeterColor).l,a3

	org $de4e
	blt.s   $DE62

; ---------------------------------------------------------------------------


 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; VARIABLES
; ---------------------------------------------------------------------------

; invulnuaribility
OCINV       equ $FFFFFD70 ;b
INTERCNT	equ $FFFFFD72 ;w

; ---------------------------------------------------------------------------
; Enable cheats
; ---------------------------------------------------------------------------

OcCheats:
	lea    	($A10003).l,a6

	sjsr    CheatSwitch
	sjsr    Invincibility
	sjsr    Interrupter
    sjsr    InfVolt

	;if modPauseMenu = 1
	;jsr     PauseMenu
	;endif

	jmp 	$1e7a

; ---------------------------------------------------------------------------
CheatSwitch:
;    cmpi.b #$0,($FFFFFD2B).w
;	bne.s +
;
;    move.b #$1,($FFFFFD2B).w
;
; +:
;	cmpi.b #$1,($FFFFFD2B).w
;	beq.s  ++

	move.b #$C6,($FFFFFD31).w

    rts

;++:
;    move.b #$80,($FFFFFD31).w
;    rts

; ---------------------------------------------------------------------------
Invincibility:
    move.b #$99,($FFFFFD44).w
    cmpi.b #$1,(OCINV).w
    bne.s  locret

    move.w #5,($FFFFFD48).w
    ;move.b #2,($FFFFFD44).w
    move.b #4,($FFFFFD46).w ;#$16

locret:
    rts

LifeMeterColor:
    dc.l lmcRed        ; 0
    dc.l lmcRed        ; 1
    dc.l lmcYellow     ; 2
    dc.l lmcBlue       ; 3
    dc.l lmcGreen      ; 4

lmcGreen:   dc.w $A0
lmcBlue:    dc.w $F00               ; DATA XREF: ROM:off_0_DEA2o
lmcYellow:  dc.w $FF                ; DATA XREF: ROM:off_0_DEA2o
lmcRed:     dc.w $F                 ; DATA XREF: ROM:off_0_DEA2o


; ---------------------------------------------------------------------------
Interrupter:
	cmp.b  #$80,(PAD1).w
	beq    timeIntr
	clr.w  (INTERCNT).w
	rts

timeIntr:
	cmp.w  #(64),(INTERCNT).w
	beq	   performInterrupting
	addq.w #1,(INTERCNT).w
	rts

performInterrupting:
 if modPauseMenu
 	clr.l   (PAD1).w
	clr.w   (INTERCNT).w
	clr.b   ($FFFFFB86).w
    move.l  $4,a0
    jmp     (a0)
	;jmp    msmExit
 endif
	rts

; ---------------------------------------------------------------------------
PM0INFVOLT   equ $FFFFFD83
InfVolt:
    tst.b  (PM0INFVOLT).w
    bne    +
    rts
+:
    bset   #7,($FFFFFA31).w
    rts

; ---------------------------------------------------------------------------
; Options prints
; ---------------------------------------------------------------------------
OptionsPrints:
    lea     off_0_280E0(pc,d0.w),a1
    btst    #3,($FFF810).l
    jmp     $708E

off_0_280E0:    
	dc.l $7232         ; 0
    dc.l $724A         ; 0
    dc.l $70CE         ; 0
    dc.l $70CE         ; 0
    dc.l $739E         ; 0
    dc.l $73B8         ; 0
    dc.l $735A         ; 0
    dc.l $736E         ; 0
    dc.l $73DC         ; 0
    dc.l $73EA         ; 0
    dc.l $735A         ; 0
    dc.l $73CE         ; 0
    dc.l $7406         ; 0
    dc.l $7414         ; 0
    dc.l $735A         ; 0
    dc.l $73F8         ; 0
    dc.l $70CE         ; 0
    dc.l $70CE         ; 0
    dc.l $6F34         ; 0
    dc.l $6F34         ; 0
    dc.l $7422         ; 0
    dc.l $7440         ; 0
    dc.l $70CE         ; 0
    dc.l $70CE         ; 0
    dc.l loc_0_27F42        ; 0
    dc.l loc_0_27F42        ; 0
    dc.l loc_0_27F42        ; 0
    dc.l loc_0_27F42        ; 0

loc_0_27F42:
    cmpi.b  #1,(OCINV).w
    bne.s   loc_a
    slea6  NoD
    jsr    $1FE8
    move.b  #0,(OCINV).w
    rts

loc_a:
    slea6  YeD
    jsr    $1FE8
    move.b  #1,(OCINV).w
    rts

; ---------------------------------------------------------------------------
optionsInputPrint:
    lea     ($7FC2).w,a6
    jsr     $1FE8
    lea     NoD,a6
    jsr     $1FE8
    jmp     $7EEC

; ---------------------------------------------------------------------------
NoD:
    dc.l $4C960003
    dc.b "MODE    NORMAL       ",0
YeD:
    dc.l $4C960003
    dc.b "MODE    INVINCIBILITY",0

	align 4
; ---------------------------------------------------------------------------
; Title copyright text
; ---------------------------------------------------------------------------
copyrightText:
	dc.l $4B0E0003
	dc.b "@1994 SEGA\\GAME FREAK                                           "
	dc.b "@2010-2020 SEGAMAN                                               "
	dc.b "SEGAMAN.TOP",0

	align 4

; ---------------------------------------------------------------------------
; Options cursor positions
; ---------------------------------------------------------------------------
optionsCursor1:
    dc.w $4512,    3,$3E00  ; 0
optionsCursor2:
    dc.w $4512,    3,$2000  ; 3
optionsCursor3:
    dc.w $4C92,    3,$2300  ; 6
optionsCursor4:
    dc.w $4C92,    3,$2000  ; 9


 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
