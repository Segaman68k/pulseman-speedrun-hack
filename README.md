# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is hack for Pulseman for Sega Mega Drive/Genesis

It adds features for speedrunes to make practice easier.

Features:

* In-Game menu (pause)
* Level select
* Invulnerability
* Level manipulation (restart, change, repeat, etc.)
* Player manipulation (heal, kill, teleport, etc.)
* Game restart

### How do I get set up? ###

* Clone repo
* Create "out" folder
* Start "compile.bat"
* "out" folder will hold result

### Contribution guidelines ###

Hack begins inside "Pulseman.asm"

It works by including "ModList.asm" 2 times:

* 1) for inside ROM patches (such as fixes or PC jumps)
* 2) outside ROM patches (such as code)

### Warning ###

Dont forget that every mod compiles twice in a row.

That means if you including items (such as Macros) include it insude "if endofrom = 0" <> "endif"

This will prevent macros to define twice.

### Who do I talk to? ###

* https://segaman.top/
