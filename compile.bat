@echo off

cls

set AS_MSGPATH=bin/msg
set USEANSI=n

bin\asw  -xx -c -A Pulseman.asm
IF NOT EXIST Pulseman.p goto LABLPAUSE
IF EXIST Pulseman.p goto Convert2ROM
:Convert2ROM
bin\p2bin Pulseman.p out\PulsemanSpeedrunHack.bin -l 0 -r $-$
bin\rompad.exe out\PulsemanSpeedrunHack.bin 255 0
bin\fixheadr.exe out\PulsemanSpeedrunHack.bin
del Pulseman.p
del Pulseman.h
exit /b
:LABLPAUSE

pause


exit /b
